<?php

class Role {
	/** @var array Array Containing TitleID and Title Name */
	public $titles = [
								1, 'Doctor',
								2, 'Receptionist',
	];

	/**
	 * Array containing All the roles with assigned titles.
	 * Roles are inherited by later title's.
	 * Format:
	 * TitleId => array('ValidRoles',...)
	 * @var array
	 */
	public $roles = [
		/** Doctor */
		1 => [
				'common_perm',
				'view_profile',
				'search_patient',
				'add_diagnostic',
				'view_diagnostic',
			],
		2 => [
				'common_perm',
				'view_profile_r',
				'add_patient',
			],
	];
}
?>