<?php
/**
 * SQLAutoUpdater:
 * Updates the SQL from sql/ folder.
 * One file is executed only once.
 * All Files from sql/ are executed and saved into table so that same file isn't executed twice.
 */
class SQLAutoUpdater extends DB
{
	/**
	 * Constructor:
	 * Generates Directory, Initiates DB Class.
	 * Checks for Executed SQL Files, and executes new SQL File(if any)
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct();
		/** @var array List of Files to be executed. */
		$files = [];
		/** @var string Directory of sql/ folder. */
		$dir = BC_DIR.'sql/';

		// Check if sql_version table exists.
		$result = $this->query("SHOW TABLES LIKE 'sql_version'"); 
		$tempFiles = array();

		if ($result != NULL && $result->num_rows == 0) {
			// Table doesn't exist
		} else {	// If exists, select aready executed files.
			$tempFiles[] = '1-base.sql';	// Table exists means base is already executed.
			$result = $this->query("SELECT * FROM `sql_version`");
			if ($result != NULL && $result->num_rows > 0) {
				while ($user = $result->fetch_assoc()) {
					// Save the Already executed files into $tempFiles.
					$tempFiles[] = $user['name'];
				}
			}
		}
		// Open Directory of SQL Files
		$dir_contents = opendir($dir);
		// Generate Files in sql/ folder.
		while (false != ($file = readdir($dir_contents))) {
			// Ignore some common files.
			if ($file != "." && $file != "..") {
				// Exclude the already executed files.
				if (!in_array($file, $tempFiles)) {
					$files[] = $file;
				}
			}   
		}

		// Sort The Files to be exeucted in Alphabetically order(case-insensitive.)
		natcasesort($files);

		// Execute and Put file names into DB
		for ($i = 0; $i < count($files); $i++) {
			$fileName = $files[$i];
			// Read Contents of SQL
			$contents = file_get_contents($dir . $fileName);

			// Start Transaction for Atomicity
			$this->begin_transaction();
			// $this->begin_transaction(MYSQLI_TRANS_START_READ_WRITE); // If MySQL >= 5.6 is installed

			if ($this->multi_query($contents) != NULL) {
				// Insert Entry
				if ($fileName != "1-base.sql") {
					$this->query("INSERT INTO `sql_version` (`name`) VALUES ('$fileName');");
				}
				// Commit the Transaction
				$this->commit();
			} else {
				// Rollback Transaction
				$this->rollback();
				throw new Exception("Cannot Perform updates of {$fileName}.");
			}
		}

		return;
	}
}
?>
