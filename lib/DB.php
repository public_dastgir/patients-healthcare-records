<?php 
/**
 * Database Class
 * Autoconnects to config given.
 */

class DB extends mysqli
{
	/**
	 * Constructor
	 * Connects the MySQL Database.
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB, MYSQL_PORT);
		if ($this->connect_errno > 0) {
            die('Connect Error (' . $this->connect_errno .'): '. $this->connect_error);
        }
	}

	/**
	 * Gives the ErrorNumber with Error Message.
	 * Useful if someone just wants to trim the connect_error
	 * @method generateErrorMessage
	 * @return string               Error Message
	 */
	public function generateErrorMessage()
	{
		return('(' . $this->errno . ') '. $this->error);
	}
}
?>