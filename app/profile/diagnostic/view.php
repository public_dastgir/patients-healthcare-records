<head>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_HOME_DOCTOR'); ?>">
	<style>
	

	</style>
</head>
<div id="page">
		<div id="logo">
			<h1 style="font-family: Arial;">View Patient Details</h1>
		</div>
		<form action="<?php echo getRedirectUrl("RSYSTEM_SEARCH"); ?>" method="POST">
			<div class="col-lg-5">
				<div class="input-group input-group-lg">
		  			<span class="input-group-addon" id="sizing-addon1">Aadhar Number</span>
					<input type="text" name="ano" class="form-control" placeholder="Aadhar Number" aria-describedby="sizing-addon1">
				</div>
			</div>
			<div class="col-lg-5">
				<div class="input-group input-group-lg">
		  			<span class="input-group-addon" id="sizing-addon1">PID</span>
					<input type="text" name="pid" class="form-control" placeholder="Patient ID" aria-describedby="sizing-addon1">
				</div>
			</div>
			<div class="btn-group-lg">
				<button type="submit" class="btn btn-info">Search</button>
				<a href="<?php echo getRedirectUrl("RSYSTEM_LOGOUT"); ?>" class="btn btn-danger" role="button">Logout</a>
			</div>
		</form>
		<br/>
		<?php
				$errors = $message->getError();
				if (count($errors) > 0) {
					echo "<h4>Error:</h4>";
					foreach ($errors as $error) {
						echo "<h5>". $error. "</h5><br/>";
					}
					echo "<br/><br/>";
					$message->clearError();
				}
				$infos = $message->getInfo();
				if (count($infos) > 0) {
					echo "<h4>Info:</h4>";
					foreach ($infos as $info) {
						echo "<h5>". $info. "</h5><br/>";
					}
					echo "<br/><br/>";
					$message->clearInfo();
				}
?>
		<div id="content">
			<h2>Patient History</h2>
<?php
			$result = $DB->query("SELECT `p`.`id`, `p`.`patient_id`,`p`.clinic_name, `p`.`date`, `login`.`name` FROM `patient_visit` AS `p` LEFT JOIN `login` ON `p`.`doctor_id`=`login`.`id` WHERE `p`.`patient_id`='". $_SESSION['search_client']['pid'] ."'");
			if ($result != NULL && $result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
?>
					<span class="input-group-addon" id="basic-addon1">Visit</span>
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1">Date</span>
			  			<input type="text" name="disease" disabled class="form-control" placeholder="<?php echo $row['date']; ?>" aria-describedby="basic-addon1">
					</div>
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1">Doctor Name</span>
			  			<input type="text" name="disease" disabled class="form-control" placeholder="<?php echo $row['name']; ?>" aria-describedby="basic-addon1">
					</div>
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1">Clinic Name</span>
			  			<input type="text" name="disease" disabled class="form-control" placeholder="<?php echo $row['clinic_name']; ?>" aria-describedby="basic-addon1">
					</div>
<?php
					$result2 = $DB->query("SELECT `p`.`med_id`, `m`.`name`, `m`.`company_name` FROM `patient_prescription` AS `p` LEFT JOIN `medicine` AS `m` ON `m`.`id`=`p`.`med_id` WHERE `p`.`uid`='". $row['id'] ."'");
					if ($result2 != NULL && $result2->num_rows > 0) {
						echo '<span class="input-group-addon" id="basic-addon1">Prescriptions</span>';
						while ($row2 = $result2->fetch_assoc()) {
?>
							<div class="input-group">
					  			<span class="input-group-addon" id="basic-addon1">Medicine Name</span>
					  			<input type="text" name="disease" disabled class="form-control" placeholder="<?php echo $row2['name'].'('.$row2['company_name'].')'; ?>" aria-describedby="basic-addon1">
							</div>
<?php
						}
					}
					echo "<br/>";
				}
			}
?>
		
		
	</div>
