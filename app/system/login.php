<?php
/** Check if Post Request */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	/** Check if Username/Password is passed. */
	if (!isset($_POST['username']) || empty($_POST['username']) ||
		!isset($_POST['password']) || empty($_POST['password']) ||
		!isset($_POST['cname']) || empty($_POST['cname'])
		) {
		$message->addError("Please fill all the fields.");
		redirect('RUSER_INDEX');
	}

	$username = $DB->real_escape_string($_POST['username']);
	$pass = $DB->real_escape_string($_POST['password']);
	
	// Select all Required Fields
	$query = "SELECT `id`, `password`, `name`,  `role`, `verified`, `unique_id`, `reg_no`, `smc`, `mobile_no` FROM `login` WHERE `username` = '$username'";

	$result = $DB->query($query);

	// Get the Result
	if ($result != NULL && $result->num_rows == 1) {
		$row = $result->fetch_assoc();
	} else {
		if ($result == NULL) {
			$message->addError("DB Error: ". $DB->generateErrorMessage());
		} else {
			$message->addError("Invalid Username/Password Provided.");
		}
		redirect('RUSER_INDEX');
	}

	// Check if Verified
	if (intval($row['verified']) != 1) {
		$message->addError("Your Account has not been yet verified by Medical Council.");
		$message->addError("Please contact Medical Council for verification.");
		redirect('RUSER_INDEX');
	}
	
	// Verify the Password, if failed, Redirect back.
	if (!password_verify($pass, $row['password'])) {
		$message->addError("Invalid Username/Password.");
		redirect('RUSER_INDEX');
	}

	// Get Client IP
	$user->ip = getClientIP();

	// Store Neccessary Information in user class.
	$user->id = $row['id'];
	$user->role = intval($row['role']);
	$user->name = $row['name'];
	$user->smc = $row['smc'];
	$user->reg_no = $row['reg_no'];
	$user->cname = $_POST['cname'];
	$user->mobile = $_POST['mobile_no'];

	// Check if password needs to be rehashed, if yes, rehash and save it.
	if (password_needs_rehash($row['password'], PASSWORD_BCRYPT)) {
		$newHash = password_hash($pass, PASSWORD_BCRYPT);
		$DB->query("UPDATE `login` SET `password`='$newHash' WHERE `user`='$username'");
	}

	/** Session Id's are saved for use at only 1 pc */
	if (!empty($row['unique_id'])) {
		// Create New Session ID
		session_regenerate_id();
		$session_id = session_id();
	} else {
		$session_id = session_id();
	}
	// Set auth to true
	$_SESSION['auth'] = true;
	$_SESSION['user'] = serialize($user);


	/** Update the unique_id */
	$DB->query("UPDATE `login` SET `unique_id`='{$session_id}' AND `last_ip`='{$user->ip}' AND `last_login`='NOW()' WHERE `id`='{$row['id']}'");

	if ($user->role == 1)
		redirect('RUSER_PROFILE');
	else
		redirect('RUSER_PROFILE_R');
} else {
	$message->addError("Invalid Request");
	redirect('RUSER_INDEX');
}
?>
