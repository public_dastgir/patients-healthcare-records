<?php
/**
 * Checks Post Request
 * @method checkPost
 */
function checkPost()
{
	global $message;
	$names = ['username', 'password', 'cpassword', 'name', 'reg_no', 'smc', 'email', 'mobile_number', 'role'];
	foreach ($names as $name) {
		if (!isset($_POST[$name]) || empty($_POST[$name])) {
			$message->addError("Please fill all the required fields.");
			redirect('RUSER_INDEX');
		}
	}
	return $_POST;
}

/** Check if Post Request */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	/** Check the Post Requests */
	$field = checkPost();

	/** Validation */
	// Check if both passwords are same
	if ($field['password'] != $field['cpassword']) {
		$message->addError("Passwords do not match.");
		redirect("RUSER_INDEX");
	}
	// Validate Phone Number
	if (!preg_match('/^\d{10}$/', $field['mobile_number'])) {
		$message->addError("Invalid Mobile Number.");
		redrect("RUSER_INDEX");
	}

	$newHash = password_hash($field['password'], PASSWORD_BCRYPT);

	$username = $DB->real_escape_string($_POST['username']);
	$name = $DB->real_escape_string($_POST['name']);
	$reg = $DB->real_escape_string($_POST['reg_no']);
	$smc = $DB->real_escape_string($_POST['smc']);
	$email = $DB->real_escape_string($_POST['email']);
	$mobile = $DB->real_escape_string($_POST['mobile_number']);
	$role = $DB->real_escape_string($_POST['role']);
	$query = "INSERT INTO `login` (`username`, `password`, `name`, `reg_no`, `smc`, `email`, `mobile_no`, `role`, `verified`) VALUES ('{$username}', '{$newHash}', '{$name}', '{$reg}', '{$smc}', '{$email}', '{$mobile}', '{$role}', '0')";

	if ($DB->query($query) == NULL) {
		$message->addError("Username Already Exists");
		redirect('RUSER_INDEX');
	}

	$message->addInfo("Sign Up Successful");
	redirect('RUSER_INDEX');
} else {
	$message->addError("Invalid Request");
	redirect('RUSER_INDEX');
}
?>
