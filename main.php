<?php
require_once("config/main.php");

function getIncludeFile()
{
	global $redirect_indexes, $Role, $user, $MAIN_title;
	// Check Authentication and Role of User
	if (isset($_SESSION) && isset($_SESSION['auth'])) {
		$auth = $_SESSION['auth'];
		$userRole = 0;
		if (isset($_SESSION['user'])) {
			/** Debug Remove */
			if (gettype($_SESSION['user']) == "array") {
				session_destroy();
			}
			$user = unserialize($_SESSION['user']);
		}
	} else {
		$auth = false;
	}
	// Get the requested url.
	$url = $_SERVER['REQUEST_URI'];
	// Remove '/BASE_DIR_NAME/' from URL
	$subUrl = substr($url, strlen(BC_PATH));
	$passed = false;
	$userTitle = -1;
	// Check All Redirect Indexes to find best possible match.
	for ($i = 0; $i < count($redirect_indexes); $i = $i+1) {
		$testUrl = '/'.str_replace('/', '\/', $redirect_indexes[$i][1]).'/';
		$roleRequired = $redirect_indexes[$i][2];
		// Title of Page
		if ($redirect_indexes[$i][3] != '') {
			$MAIN_title = $redirect_indexes[$i][3];
		}
			// Match the String.
		if (preg_match($testUrl, $subUrl)) {
			// Assume Passed
			$passed = false;
			// no role required, anyone can pass.
			if ($roleRequired == '') {
				// If Logged in, redirect to profile
				if ($auth == true) {
					redirect('RUSER_PROFILE');
				}
				$passed = true;
				break;
			}

			// Role is required, check if logged in.
			if ($auth != true) {
				redirect('RUSER_INDEX');
			}

			// Get User Roles
			if ($user->role != 0 && in_array($roleRequired, $Role->roles[$user->role])) {
				$passed = true;
				break;
			} else {
				$passed = false;
				break;
			}
		}
	}
	if ($passed == false) {	// Invalid Page/Unauthorized
		return('app/error404.php');
	} else {
		// Authorized, get the Template Name.
		$fileToInclude = 'app/'. substr($subUrl, 0, -1).'.php';
		// If File doesn't exist, show 404 page.
		if (!file_exists($fileToInclude)) {
			return('app/error404.php');
		} else {
			// Include the Template
			return($fileToInclude);
		}
	}
}

$MAIN_title = "Paper Evaluation System";

/// Start Session
session_start();
/// Initialize DB
$Role = new Role();
$DB = new DB();
$message = new Message();
$user = new User();

if (defined('SQL_AUTO_UPDATE') && SQL_AUTO_UPDATE == true) {
	$SQLAutoUpdater = new SQLAutoUpdater();
}

$fileName = getIncludeFile();
include 'app/header.php';
/// Include the File
include $fileName;
include 'app/footer.php';

/// Close DB
$DB->close();
?>
