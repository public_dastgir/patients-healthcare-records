<html>
<head>
	<meta charset="UTF-8">
	<title>Add Patient Details</title>
    <link rel='stylesheet prefetch' href='<?php echo getRedirectUrl('RCSS_BOOTSTRAP'); ?>'>
    <link rel='stylesheet prefetch' href='<?php echo getRedirectUrl('RCSS_BOOTSTRAP_T'); ?>'>
    <link rel='stylesheet prefetch' href='<?php echo getRedirectUrl('RCSS_BOOTSTRAP_V'); ?>'>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_HOME_RECEPT'); ?>">
</head>
<body>
	<div class="container">
		<form class="well form-horizontal" action="<?php echo getRedirectUrl('RSYSTEM_ADD_PATIENT'); ?>" method="post"	id="contact_form">
			<fieldset>
				<!-- Form Name -->
				<span>
					<a style="position: absolute; right: 200px;" href="<?php echo getRedirectUrl("RSYSTEM_LOGOUT"); ?>" class="btn btn-danger" role="button">Logout</a>
					<legend>Patient Details</legend>
				</span>
<?php
				$errors = $message->getError();
				if (count($errors) > 0) {
					echo "<h4>Error:</h4><br/>";
					foreach ($errors as $error) {
						echo $error. "<br/>";
					}
					echo "<br/><br/>";
					$message->clearError();
				}
				$infos = $message->getInfo();
				if (count($infos) > 0) {
					echo "<h4>Info:</h4><br/>";
					foreach ($infos as $info) {
						echo $info. "<br/>";
					}
					echo "<br/><br/>";
					$message->clearInfo();
				}
?>
				<h5>Please Enter only Aadhar Card.</h5>
				<h6>If Aadhar Card is not available, please fill other details.</h6>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">First Name</label>	
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input	name="first_name" placeholder="First Name" class="form-control"	type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" >Last Name</label> 
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-user"></i>
							</span>
							<input name="last_name" placeholder="Last Name" class="form-control"	type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
 				<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
      			<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      			<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      			<!-- Javascript -->
<script>
$(function() {
	$( "#datepicker-13" ).datepicker();
	$( "#datepicker-13" ).datepicker("show");
	});
</script>
				<div class="form-group">
					<label class="col-md-4 control-label" >Date of Birth</label> 
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input type="text" name="dob" id="datepicker-13" class="form-control">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" >Aadhar Card Number</label> 
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input name="ano" placeholder="1234567812345678" class="form-control"	type="text">
						</div>
					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" >Blood Group</label> 
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input name="bg" placeholder="O+" class="form-control"	type="text">
						</div>
					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">E-Mail</label>	
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
							<input name="email" placeholder="E-Mail Address" class="form-control"	type="text">
						</div>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label">Phone #</label>	
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
							<input name="phone" placeholder="9123456789" class="form-control" type="text">
						</div>
					</div>
				</div>

				<!-- Text input-->			
				<div class="form-group">
					<label class="col-md-4 control-label">Address</label>	
					<div class="col-md-4 inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
							<input name="address" placeholder="Address" class="form-control" type="text">
						</div>
					</div>
				</div>

				<!-- Button -->
				<div class="form-group">
					<div class="form-group">
						<label class="col-md-5 control-label"></label>
						<div class="col-md-5">
							<button type="submit" class="btn btn-warning" >Save Details <span class="glyphicon glyphicon-send"></span></button>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div><!-- /.container -->

<script src='<?php echo getRedirectUrl('RJS_HOME_R'); ?>'></script>
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
<script src="js/index.js"></script>

</body>
</html>
