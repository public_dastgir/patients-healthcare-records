<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login Page</title>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
	<link rel='stylesheet prefetch' href='<?php echo getRedirectUrl('RCSS_FONTAWESOME'); ?>'>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_STYLE'); ?>">


	
</head>

<body>
	
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title
<div class="pen-title">
	<h1>Flat Login Form</h1><span>Pen <i class='fa fa-paint-brush'></i> + <i class='fa fa-code'></i> by <a href='http://andytran.me'>Andy Tran</a></span>
</div>
-->
<!-- Form Module-->
<div class="module form-module">
	<div class="toggle"><i class="fa fa-times fa-pencil"></i>
		<div class="tooltip">Sign Up/Login</div>
	</div>
	<div class="form">
		<?php
			$errors = $message->getError();
			//var_dump($_SESSION);
			if (count($errors) > 0) {
				echo "<h4>Error:</h4><br/>";
				foreach ($errors as $error) {
					echo $error. "<br/>";
				}
				echo "<br/><br/>";
				$message->clearError();
			}
			$infos = $message->getInfo();
			if (count($infos) > 0) {
				echo "<h4>Info:</h4><br/>";
				foreach ($infos as $info) {
					echo $info. "<br/>";
				}
				echo "<br/><br/>";
				$message->clearInfo();
			}
		?>
		<h2>Login to your account</h2>
		<form method="post" action="<?php echo getRedirectUrl('RSYSTEM_LOGIN'); ?>">
			<input type="text" name="username" placeholder="Username"/>
			<input type="password" name="password" placeholder="Password"/>
			<input type="text" name="cname" placeholder="Clinic/Hospital Name"/>
			<button>Login</button>
		</form>
	</div>
	<div class="form">
		<h2>Create an account</h2>
		<form method="post" action="<?php echo getRedirectUrl('RSYSTEM_SIGNUP'); ?>">
		<!--
		-->
			<input type="text" name="username" placeholder="Username"/>
			<input type="password" name="password" placeholder="Password"/>
			<input type="password" name="cpassword" placeholder="Confirm Password"/>
			<input type="text" name="name" placeholder="Name"/>
			<input type="text" name="reg_no" placeholder="Registration Number"/>
			<input type="text" name="smc" placeholder="State Medical Council Name"/>
			<input type="text" name="email" placeholder="Email Address"/>
			<input type="tel" name="mobile_number" placeholder="Mobile Number"/>
			<div class="select-style">
				<select name="role">
					<option value="1">Doctor</option>
					<option value="2">Receptionist</option>
				</select>
			</div>
			<br/>
			<button>Register</button>
		</form>
	</div>
	
</div>


	<script src="<?php echo getRedirectUrl('RJS_JQUERY'); ?>"></script>
	<script src="<?php echo getRedirectUrl('RJS_BOOTSTRAP'); ?>"></script>
	<script src="<?php echo getRedirectUrl('RJS_INDEX'); ?>"></script>
	<link href="<?php echo getRedirectUrl('RCSS_LOGIN_F'); ?>" rel="stylesheet">
	<link href="<?php echo getRedirectUrl('RCSS_LOGIN_1'); ?>" rel="stylesheet">

</body>
</html>
