<?php
function rS($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

/** Check if Post Request */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (!isset($_POST['ano']) || empty($_POST['ano'])) {
		if (
			!isset($_POST['first_name']) || empty($_POST['first_name']) ||
			!isset($_POST['last_name']) || empty($_POST['last_name']) ||
			!isset($_POST['bg']) || empty($_POST['bg']) ||
			!isset($_POST['email']) || empty($_POST['email']) ||
			!isset($_POST['phone']) || empty($_POST['phone']) ||
			!isset($_POST['address']) || empty($_POST['address']) ||
			!isset($_POST['dob']) || empty($_POST['dob'])
			) {
			$message->addError("Please fill all the fields.");
			redirect('RUSER_PROFILE_R');
		}
		$firstName = $_POST['first_name'];
		$lastName = $_POST['last_name'];
		$bloodGroup = $_POST['bg'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$dob = $_POST['dob'];
		$aadhar = '';
	} else {
		$firstName = rS();
		$lastName = rS();
		$bloodGroup = 'A';
		$email = rS(6)."@gmail.com";
		$phone = "9123456789";
		$address = rS(50);
		$dob = '2017-03-20';
		$aadhar = $_POST['ano'];
	}

	$patientId = rS();

	$query = "INSERT INTO `user_details` (`patient_id`, `first_name`, `last_name`, `dob`, `aadhar`, `blood_group`, `email`, `mobile_no`, `address`) VALUES ('{$patientId}', '{$firstName}', '{$lastName}', STR_TO_DATE('{$dob}', '%Y-%m-%d'), '{$aadhar}', '{$bloodGroup}', '{$email}', '{$phone}', '{$address}')";
	
	if ($DB->query($query) !== TRUE) {
		$message->addError("<font color='red'>Same Aadhar card Exist.</font>");
		redirect('RUSER_PROFILE_R');
	} else {
		$message->addInfo("User Details Successfully Added.");
		$message->addInfo("<font color='blue'>Patient Id: {$patientId}</font>");
		redirect('RUSER_PROFILE_R');
	}
} else {
	$message->addError("Invalid Request");
	redirect('RUSER_PROFILE_R');
}
?>