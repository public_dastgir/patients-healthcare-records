
INSERT INTO `compounds` (`id`, `medicine_id`, `compound_name`, `mg`) VALUES
(1, 5, 'Paracetamol', 500),
(2, 6, 'Paracetamol', 500),
(3, 7, 'Caffeine', 30),
(4, 8, 'Caffeine', 30),
(5, 7, 'Chlorpheniramine', 2),
(6, 8, 'Chlorpheniramine', 2),
(7, 7, 'Paracetamol', 500),
(8, 8, 'Paracetamol', 500),
(9, 7, 'Phenylephrine', 10),
(10, 8, 'Phenylephrine', 10);

--
-- Dumping data for table `medicine`
--

INSERT INTO `medicine` (`id`, `name`, `company_name`, `price`, `uses`, `side_effects`) VALUES
(5, 'CROCIN ADVANCE 500 MG TABLET', 'GSK', 15.91, 'Crocin advance 500 mg tablet is used in fever, headache, pain during menstruation, joint pain (arthralgia), muscle pain (myalgia), dental pain and post operative pain', 'Allergic reaction, Liver damage'),
(6, 'PARACETAMOL 500 MG TABLET', 'Jagsonpal Pharmaceuticals Ltd', 137.5, 'Paracetamol 500 mg tablet is used in fever, headache, pain during menstruation, joint pain (arthralgia), muscle pain (myalgia), dental pain and post operative pain', 'Paracetamol 500 mg tablet is used in fever, headache, pain during menstruation, joint pain (arthralgia), muscle pain (myalgia), dental pain and post operative pain'),
(7, 'SINAREST TABLET', 'Centaur Pharmaceuticals Pvt Ltd', 38.2, 'Caffeine is used in the treatment of migraine. Chlorpheniramine is used in the treatment of allergic disorders', 'Nausea, Vomiting, Headache, Palpitations, Increased blood pressure, Increased heart rate, Heart beat irregular'),
(8, 'MACBERY COLD TABLET', 'Macleods Pharmaceuticals Pvt Ltd', 26.5, 'Caffeine is used in the treatment of migraine. Chlorpheniramine is used in the treatment of allergic disorders', 'Insomnia (difficulty in sleeping), Dehydration');
