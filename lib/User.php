<?php
/**
 * User Class (Variables)
 * Should only contain variables that are required to be saved in sessions.
 */

class User
{
	/**
	 * User ID
	 * @var int
	 */
	public $id = 0;
	/**
	 * Role of User
	 * @see /lib/Role.php
	 * @var int
	 */
	public $role = 0;
	/**
	 * Name of User
	 * @var string
	 */
	public $name = "Unknown";
	/**
	 * IP from which user is logged in.
	 * @var string
	 */
	public $ip = "0.0.0.0";
}
?>
