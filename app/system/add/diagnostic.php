<html>
<head>
	<title>Add Patient Report</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
	<link rel="stylesheet" href="/bitcamp/css/addi/">
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_HOME_DOCTOR'); ?>">
</head>

<div id="page">
	<div id="logo">
		<h1 style="font-family: Arial;">View Patient Details</h1>
	</div>
	<form action="<?php echo getRedirectUrl("RSYSTEM_SEARCH"); ?>" method="POST">
		<div class="col-lg-5">
			<div class="input-group input-group-lg">
	  			<span class="input-group-addon" id="sizing-addon1">Aadhar Number</span>
				<input type="text" name="ano" class="form-control" placeholder="Aadhar Number" aria-describedby="sizing-addon1">
			</div>
		</div>
		<div class="col-lg-5">
			<div class="input-group input-group-lg">
	  			<span class="input-group-addon" id="sizing-addon1">PID</span>
				<input type="text" name="pid" class="form-control" placeholder="Patient ID" aria-describedby="sizing-addon1">
			</div>
		</div>
		<div class="btn-group-lg">
			<button type="submit" class="btn btn-info">Search</button>
			<a href="<?php echo getRedirectUrl("RSYSTEM_LOGOUT"); ?>" class="btn btn-danger" role="button">Logout</a>
		</div>
	</form>
	<br/>
	<?php
			$errors = $message->getError();
			if (count($errors) > 0) {
				echo "<h4>Error:</h4>";
				foreach ($errors as $error) {
					echo "<h5>". $error. "</h5><br/>";
				}
				echo "<br/><br/>";
				$message->clearError();
			}
			$infos = $message->getInfo();
			if (count($infos) > 0) {
				echo "<h4>Info:</h4>";
				foreach ($infos as $info) {
					echo "<h5>". $info. "</h5><br/>";
				}
				echo "<br/><br/>";
				$message->clearInfo();
			}
	?>
	<div id="content">
		<h1>Information</h1>
		<br/>
		<div id="printableArea">
<?php
		$DB->query("INSERT INTO `patient_visit` (patient_id, doctor_id, clinic_name) VALUES ('". $_SESSION['search_client']['pid'] ."', '". $user->id ."', '". $user->cname ."')");
		$insertedId = $DB->insert_id;
		foreach ($_POST as $key => $val) {
			if ($key == "disease") {
				echo '<span class="input-group-addon" id="basic-addon1">Disease: '. $val .'</span><br/>';
			} else {
				if ($val == "on") {
					$medId = intval($key);
					$DB->query("INSERT INTO `patient_prescription` (uid, med_id) VALUES ('". $insertedId ."', '". $medId ."')");;
					$result = $DB->query("SELECT `name`, `company_name`, `uses`, `side_effects`, `price` FROM medicine WHERE `id`='$medId'");
					if ($result != NULL && $result->num_rows == 1) {
						$row = $result->fetch_assoc();
						echo '<span class="input-group-addon" id="basic-addon1">Medicine: '. $row['name'] .'('. $row['company_name'] .')</span>';
?>
						<div class="input-group">
			  				<span class="input-group-addon" id="basic-addon1">Price</span>
			  				<input type="text" class="form-control" disabled placeholder="<?php echo $row["price"]; ?>" aria-describedby="basic-addon1">
						</div>
						<div class="input-group">
			  				<span class="input-group-addon" id="basic-addon1">Uses</span>
			  				<input type="text" class="form-control" disabled placeholder="<?php echo $row["uses"]; ?>" aria-describedby="basic-addon1">
						</div>
						<div class="input-group">
			  				<span class="input-group-addon" id="basic-addon1">Side Effects</span>
			  				<input type="text" class="form-control" disabled placeholder="<?php echo $row["side_effects"]; ?>" aria-describedby="basic-addon1">
						</div>
<?php
						echo '<span class="input-group-addon" id="basic-addon1">Compounds</span>';
						$compounds = [];
						$mgs = [];
						$result = $DB->query("SELECT `compound_name`, `mg` FROM `compounds` WHERE `medicine_id`='". $medId ."'");
						if ($result != NULL && $result->num_rows > 0) {
							while ($row = $result->fetch_assoc()) {
								$compounds[] = $row['compound_name'];
								$mgs[] = $row['mg'];
?>
								<div class="input-group">
					  				<span class="input-group-addon" id="basic-addon1"><?php echo $row['compound_name'] ?></span>
			  						<input type="text" class="form-control" disabled placeholder="<?php echo $row['mg'].'mg'; ?>" aria-describedby="basic-addon1">
								</div>
<?php
							}
							$q = 'compound_name IN (';
							for ($i = 0; $i < count($compounds); $i++) {
								$q .= "'". $compounds[$i] ."'";
								if ($i != count($compounds)-1) {
									$q .= ",";
								} else {
									$q .= ')';
								}
							}
							$result = $DB->query("SELECT `medicine_id`, (SELECT COUNT(*) FROM `compounds` `c` WHERE `c`.`medicine_id`=`p`.medicine_id) AS `count` FROM `compounds` `p` WHERE ". $q ." group by `medicine_id` HAVING count(distinct `compound_name`)=". count($compounds));
							if ($result != NULL && $result->num_rows > 0) {
								while ($row = $result->fetch_assoc()) {
									if (intval($row['medicine_id']) == $medId || intval($row['count']) != count($compounds))
										continue;
									echo '<span class="input-group-addon" id="basic-addon1">Alternatives</span>';
									$result2 = $DB->query("SELECT `name`, `company_name`, `price` FROM medicine WHERE `id`='{$row['medicine_id']}'");
									if ($result2 != NULL && $result2->num_rows > 0) {
										while ($row2 = $result2->fetch_assoc()) {
?>
											<div class="input-group">
						  						<span class="input-group-addon" id="basic-addon1">Alt Medicine: <?php echo $row2['name'].'('.$row2['company_name'].')'; ?></span>
					  							<input type="text" class="form-control" disabled placeholder="Price: <?php echo $row2['price']; ?>" aria-describedby="basic-addon1">
											</div>
<?php
										}
									}
								}
							}
?>
							<div class="input-group">
				  				<span class="input-group-addon" id="basic-addon1">Additional Comments</span>
				  				<input type="text" class="form-control" disabled placeholder="" aria-describedby="basic-addon1">
				  				<input type="text" class="form-control" disabled placeholder="" aria-describedby="basic-addon1">
							</div>
<?php
						}
					}
				}
			}
		}
?>
		</div>
		<br/>
		<input style="margin-left: 45%;" type="button" onclick="printDiv('printableArea')" value="Print Receipt" />
		<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
		<script src='http://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js'></script>
<script>
// Events
$('.dropdown-container')
	.on('click', '.dropdown-button', function() {
    	$('.dropdown-list').toggle();
	})
	.on('input', '.dropdown-search', function() {
    	var target = $(this);
    	var search = target.val().toLowerCase();
    
    	if (!search) {
            $('li').show();
            return false;
        }
    
    	$('li').each(function() {
        	var text = $(this).text().toLowerCase();
            var match = text.indexOf(search) > -1;
            $(this).toggle(match);
        });
	})
	.on('change', '[type="checkbox"]', function() {
    	var numChecked = $('[type="checkbox"]:checked').length;
    	$('.quantity').text(numChecked || 'Any');
	});

// JSON of States for demo purposes
var usStates = [
<?php
	$result = $DB->query("SELECT `id`,`name`, `company_name` FROM `medicine`");
	if ($result != NULL && $result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			echo " { name: '{$row['name']}: {$row['company_name']}', abbreviation: '{$row['id']}'},";
		}
	}
?>
];

// <li> template
var stateTemplate = _.template(
    '<li>' +
    	'<input name="<%= abbreviation %>" type="checkbox">' +
    	'<label for="<%= abbreviation %>"><%= capName %></label>' +
    '</li>'
);

// Populate list with states
_.each(usStates, function(s) {
    s.capName = _.startCase(s.name.toLowerCase());
    $('ul').append(stateTemplate(s));
});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
	</div>
	</div>
</body>
</html>
