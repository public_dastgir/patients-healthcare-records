<?php

/**
 * Base Directory of Bitcamp
 */
define('BC_DIR', dirname(__FILE__)."/../");
/**
 * Bitcamp Directory from site
 */
define('BC_PATH', '/bitcamp/');
/**
 * Site Owner Email
 */
define('SITE_OWNER_EMAIL', 'dastgirp@gmail.com');
/**
 * Auto Update SQL from SQL folder?
 * @note Ony use on debug mode.
 */
define('SQL_AUTO_UPDATE', true);

/** Constants */
define('MESSAGE_DEBUG', 1);
define('MESSAGE_ERROR', 2);
define('MESSAGE_INFO', 4);



/** Functions */

/**
 * Autoload Module if not already loaded.
 * Checks for module first, then check for trait.
 * @method __autoload
 * @param  string	 $classname ClassName
 */
function __autoload($classname)
{
	$filename = BC_DIR . 'lib/'. $classname . '.php';
	if (file_exists($filename) == NULL) {
		debug_print_backtrace();
		throw new Exception("Cannot find $classname.");
	}
	require_once($filename);
}

/**
 * Returns the ClientIP
 * @method getClientIP
 * @return string	  IP Address(IPv4/IPv6)
 */
function getClientIP()
{
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	} else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	} else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	} else if (isset($_SERVER['HTTP_FORWARDED'])) {
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	} else if (isset($_SERVER['REMOTE_ADDR'])) {
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	} else {
		throw new Exception("Error(Code: PI), Please Contact Site Administrator(". SITE_OWNER_EMAIL .")");
	}
 
	return $ipaddress;
}

/**
 * Gives RedirectUrl from constant
 * @method getRedirectUrl
 * @param  string         $redirectUniqueString Redirect Constant
 * @return string                               Fully Qualified URL
 */
function getRedirectUrl($redirectUniqueString)
{
	if (!defined($redirectUniqueString)) {
		throw new Exception("getRedirectUrl: Invalid Unique Constant: {$redirectUniqueString}.");
	}
	return BC_PATH.constant($redirectUniqueString);
}

/**
 * Registers Redirect Constants from $redirect_indexes
 * @method registerRedirectConstants
 */
function registerRedirectConstants()
{
	global $redirect_indexes;
	for ($i = 0; $i < count($redirect_indexes); $i++) {
		if (defined($redirect_indexes[$i][0])) {
			throw new Exception("{$redirect_indexes[$i][0]} Constant is already defined.");
		}
		define($redirect_indexes[$i][0], $redirect_indexes[$i][1]);
	}
}

/**
 * Redirects the User to provided Url
 * @method redirect
 * @param  string   $redirectUniqueString Redirect Constant
 */
function redirect($redirectUniqueString)
{
	//global $DB;
	// Close DB before redirect
	//$DB->close();
	header("Location: ". getRedirectUrl($redirectUniqueString));
	die();
}

?>