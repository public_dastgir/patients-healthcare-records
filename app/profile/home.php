<head>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_HOME_DOCTOR'); ?>">
	<style>
	

	</style>
</head>
<div id="page">
		<div id="logo">
			<h1 style="font-family: Arial;">View Patient Details</h1>
		</div>
		<form action="<?php echo getRedirectUrl("RSYSTEM_SEARCH"); ?>" method="POST">
			<div class="col-lg-5">
				<div class="input-group input-group-lg">
		  			<span class="input-group-addon" id="sizing-addon1">Aadhar Number</span>
					<input type="text" name="ano" class="form-control" placeholder="Aadhar Number" aria-describedby="sizing-addon1">
				</div>
			</div>
			<div class="col-lg-5">
				<div class="input-group input-group-lg">
		  			<span class="input-group-addon" id="sizing-addon1">PID</span>
					<input type="text" name="pid" class="form-control" placeholder="Patient ID" aria-describedby="sizing-addon1">
				</div>
			</div>
			<div class="btn-group-lg">
				<button type="submit" class="btn btn-info">Search</button>
				<a href="<?php echo getRedirectUrl("RSYSTEM_LOGOUT"); ?>" class="btn btn-danger" role="button">Logout</a>
			</div>
		</form>
		<br/>
		<?php
				$errors = $message->getError();
				if (count($errors) > 0) {
					echo "<h4>Error:</h4>";
					foreach ($errors as $error) {
						echo "<h5>". $error. "</h5><br/>";
					}
					echo "<br/><br/>";
					$message->clearError();
				}
				$infos = $message->getInfo();
				if (count($infos) > 0) {
					echo "<h4>Info:</h4>";
					foreach ($infos as $info) {
						echo "<h5>". $info. "</h5><br/>";
					}
					echo "<br/><br/>";
					$message->clearInfo();
				}
?>
		<div id="content">
			<h2>Information</h2>
			<p>
				Please Enter Aadhar Number or Patient ID to get the patient details and diagnose the patient.
			</p>
<?php
		if (isset($_SESSION) && isset($_SESSION['search_client']) && !empty($_SESSION['search_client'])) {
?>
			
				<h4>Aadhar Number: <?php echo $_SESSION['search_client']['aadhar']; ?></h4>
				<h4>Patient Id: <?php echo $_SESSION['search_client']['pid']; ?></h4>
				<h4>Patient's First Name: <?php echo $_SESSION['search_client']['pname']; ?></h4>
				<h4>Last Name: <?php echo $_SESSION['search_client']['plname']; ?></h4>
				<h4>Date of Birth: <?php echo $_SESSION['search_client']['dob']; ?></h4>
				<h4>Blood Group: <?php echo $_SESSION['search_client']['bg']; ?></h4>
				<h4>Mobile Number: <?php echo $_SESSION['search_client']['mobile_no']; ?></h4>
				<h4>Address: <?php echo $_SESSION['search_client']['address']; ?></h4>
				<br><br>
				<div class="btn-group-lg">
					<a style="margin-left:20%;" href="<?php echo getRedirectUrl("RUSER_VIEW_DIAGNOSTIC"); ?>" class="btn btn-success" role="button">View Old Patient Diagnosis</a>
					<a style="margin-left:20%;" href="<?php echo getRedirectUrl("RUSER_ADD_DIAGNOSTIC"); ?>" class="btn btn-success" role="button">Add New Diagnosis</a>
				</div>
			
<?php
		}
?>
		
	</div>
