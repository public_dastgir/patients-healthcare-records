<html>
<head>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_FONTAWESOME'); ?>">
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_STYLE'); ?>">
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_BOOTSTRAP_V'); ?>">
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_BOOTSTRAP'); ?>">

	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_BOOTSTRAP_T'); ?>">

	<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_JQUERY'); ?>"></script>
	<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_BOOTSTRAP'); ?>"></script>
</head>
<body>