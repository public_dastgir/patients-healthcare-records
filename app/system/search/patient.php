<?php
/** Check if Post Request */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (
		(!isset($_POST['ano']) || empty($_POST['ano'])) &&
		(!isset($_POST['pid']) || empty($_POST['pid']))
		) {
		$_SESSION['search_client'] = [];
		$message->addError("Please Provide either Aadhar Number or Patient Id");
		redirect('RUSER_INDEX');
	}
	$pid = '';
	$ano = '';
	if (!isset($_POST['ano']) || empty($_POST['ano'])) {
		$pid = $_POST['pid'];
		$pd = $DB->real_escape_string($pid);
		$query = "SELECT * FROM user_details WHERE `patient_id`='{$pid}'";
	} else {
		$ano = $_POST['ano'];
		$ano = $DB->real_escape_string($ano);
		$query = "SELECT * FROM user_details WHERE `aadhar`='{$ano}'";
	}
	$result = $DB->query($query);
	if ($result != NULL && $result->num_rows == 1) {
		$row = $result->fetch_assoc();
	} else {
		$_SESSION['search_client'] = [];
		$message->addError("No such ID Exists");
		redirect('RUSER_INDEX');
	}
	$_SESSION['search_client'] = [
									'aadhar' => $row['aadhar'],
									'pid' => $row['patient_id'],
									'pname' => $row['first_name'],
									'plname' => $row['last_name'],
									'dob' => $row['dob'],
									'bg' => $row['blood_group'],
									'mobile_no' => $row['mobile_no'],
									'address' => $row['address'],
								];
	redirect('RUSER_INDEX');
	exit();
} else {
	$_SESSION['search_client'] = [];
	$message->addError("Invalid Request");
	redirect('RUSER_INDEX');
}
?>