<?php 
if ($_SERVER["REQUEST_METHOD"] != "POST") {
	$message->addError("Invalid Page Requested.");
	redirect("RUSER_INDEX");
}
?>
<html >
<head>
	<title>Add Patient Report</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
	<link rel="stylesheet" href="/bitcamp/css/addi/">
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_HOME_DOCTOR'); ?>">
</head>

<div id="page">
	<div id="logo">
		<h1 style="font-family: Arial;">View Patient Details</h1>
	</div>
	<form action="<?php echo getRedirectUrl("RSYSTEM_SEARCH"); ?>" method="POST">
		<div class="col-lg-5">
			<div class="input-group input-group-lg">
	  			<span class="input-group-addon" id="sizing-addon1">Aadhar Number</span>
				<input type="text" name="ano" class="form-control" placeholder="Aadhar Number" aria-describedby="sizing-addon1">
			</div>
		</div>
		<div class="col-lg-5">
			<div class="input-group input-group-lg">
	  			<span class="input-group-addon" id="sizing-addon1">PID</span>
				<input type="text" name="pid" class="form-control" placeholder="Patient ID" aria-describedby="sizing-addon1">
			</div>
		</div>
		<div class="btn-group-lg">
			<button type="submit" class="btn btn-info">Search</button>
			<a href="<?php echo getRedirectUrl("RSYSTEM_LOGOUT"); ?>" class="btn btn-danger" role="button">Logout</a>
		</div>
	</form>
	<br/>
	<?php
			$errors = $message->getError();
			if (count($errors) > 0) {
				echo "<h4>Error:</h4>";
				foreach ($errors as $error) {
					echo "<h5>". $error. "</h5><br/>";
				}
				echo "<br/><br/>";
				$message->clearError();
			}
			$infos = $message->getInfo();
			if (count($infos) > 0) {
				echo "<h4>Info:</h4>";
				foreach ($infos as $info) {
					echo "<h5>". $info. "</h5><br/>";
				}
				echo "<br/><br/>";
				$message->clearInfo();
			}
	?>
	<div id="content">
		<h1>Information</h1>
		<p>
			Please Enter the Following Details:
		</p>
		<br/>
		<form action="<?php echo getRedirectUrl("RSYSTEM_ADD_DIAGNOSTIC"); ?>" method="POST">
			<div class="input-group">
			  <span class="input-group-addon" id="basic-addon1">Disease</span>
			  <input type="text" name="disease" class="form-control" placeholder="Disease Name" aria-describedby="basic-addon1">
			</div>
			<div class="dropdown-container">
   				<div class="dropdown-button noselect">
			        <div class="dropdown-label">Medicines</div>
       				<i class="fa fa-filter"></i>
   				</div>
   				<div class="dropdown-list" style="display: none;">
       				<input type="search" placeholder="Search Medicines" class="dropdown-search"/>
       				<ul></ul>
   				</div>
			</div>
			<br/><br/><br/>
			<div class="btn-group-lg" style="margin-left:47%">
				<button type="submit" class="btn btn-info btn-info-lg">Submit</button>
			</div>
		</form>
		<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
		<script src='http://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js'></script>
<script>
// Events
$('.dropdown-container')
	.on('click', '.dropdown-button', function() {
    	$('.dropdown-list').toggle();
	})
	.on('input', '.dropdown-search', function() {
    	var target = $(this);
    	var search = target.val().toLowerCase();
    
    	if (!search) {
            $('li').show();
            return false;
        }
    
    	$('li').each(function() {
        	var text = $(this).text().toLowerCase();
            var match = text.indexOf(search) > -1;
            $(this).toggle(match);
        });
	})
	.on('change', '[type="checkbox"]', function() {
    	var numChecked = $('[type="checkbox"]:checked').length;
    	$('.quantity').text(numChecked || 'Any');
	});

// JSON of States for demo purposes
var usStates = [
<?php
	$result = $DB->query("SELECT `id`,`name`, `company_name` FROM `medicine`");
	if ($result != NULL && $result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			echo " { name: '{$row['name']}: {$row['company_name']}', abbreviation: '{$row['id']}'},";
		}
	}
?>
];

// <li> template
var stateTemplate = _.template(
    '<li>' +
    	'<input name="<%= abbreviation %>" type="checkbox">' +
    	'<label for="<%= abbreviation %>"><%= capName %></label>' +
    '</li>'
);

// Populate list with states
_.each(usStates, function(s) {
    s.capName = _.startCase(s.name.toLowerCase());
    $('ul').append(stateTemplate(s));
});
</script>
	</div>
	</div>
</body>
</html>
