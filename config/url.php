<?php
/**
 * Contains useful information for redirects.
 * Format:
 * 	['RedirectUniqueConstant', 'RedirectUrl(FileName)', 'role_name', 'Title of Page(Blank for Default)'],
 * @var Array
 */
$redirect_indexes = [
						/** User Redirects */
						['RUSER_INDEX'    , 'index/'                , ''            , ''         ],
						['RUSER_PROFILE'  , 'profile/home/'         , 'view_profile', 'Home Page'],
						['RUSER_PROFILE_R', 'profile/home_r/'       , 'view_profile_r', 'Home Page'],
						['RUSER_ADD_DIAGNOSTIC', 'profile/diagnostic/add/', 'add_diagnostic', ''],
						['RUSER_VIEW_DIAGNOSTIC', 'profile/diagnostic/view/', 'view_diagnostic', ''],
						/** System Redirects */
						['RSYSTEM_LOGIN'  , 'system/login/'         , ''            , ''         ],
						['RSYSTEM_SIGNUP' , 'system/signup/'        , ''            , ''         ],
						['RSYSTEM_LOGOUT' , 'system/logout/'        , 'common_perm' , ''         ],
						['RSYSTEM_SEARCH' , 'system/search/patient/', 'search_patient', ''       ],
						['RSYSTEM_ADD_PATIENT', 'system/add/patient/', 'add_patient', ''],
						['RSYSTEM_ADD_DIAGNOSTIC', 'system/add/diagnostic/', 'add_diagnostic', ''],
						/** CSS Redirects */
						// ['RCSS_LOGIN',    'css/login/'       , ''            , ''         ],
						// ['RCSS_PROFILE',  'css/profile_home/', 'view_profile', ''         ],
						['RCSS_FONTAWESOME', 'css/font-awesome.min/', '', ''],
						['RCSS_STYLE'      , 'css/style/'           , '', ''],
						['RCSS_LOGIN_1'    , 'css/login/login/'     , '', ''],
						['RCSS_LOGIN_F'    , 'css/login/fonts/'     , '', ''],
						
						['RCSS_HOME_RECEPT', 'css/homer-style/'     , '', ''],
						['RCSS_BOOTSTRAP_V', 'css/bootstrapValidator.min/', '', ''],
						['RCSS_BOOTSTRAP'  , 'css/bootstrap.min/'   , '', ''],
						['RCSS_BOOTSTRAP_T', 'css/bootstrap-theme.min/', '', ''],

						['RCSS_HOME_DOCTOR', 'css/profile/home/', '', ''],
						/** JS Redirect */
						['RJS_INDEX'       , 'js/index/'            , '', ''],
						['RJS_JQUERY'      , 'js/jquery.min/'       , '', ''],
						['RJS_BOOTSTRAP'   , 'js/bootstrap.min/'    , '', ''],
						
						['RJS_HOME_R'      , 'js/homer/'       , '', ''],
						['RJS_BOOTSTRAP_V' , 'js/bootstrapvalidator.min', '', ''],
					];
?>
